(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix download)
             (guix git)
             (guix git-download)
             (guix build-system gnu)
             (guix utils)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages game-development)
             (gnu packages pkg-config)
             (gnu packages sdl)
             (gnu packages texinfo))

(define chickadee
  (let ((commit "ca8b2bd07a07e727b17ebbdf8012c7d9cd135689"))
    (package
      (inherit guile-chickadee)
      (version (string-append "0.9.0-1." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.dthompson.us/chickadee.git")
                      (commit commit)))
                (sha256
                 (base32
                  "1s6wgb58z09f5li90ds6pnmfismd81xawf0qkxr1jkmvywfdg427"))))
      (native-inputs (list autoconf automake pkg-config texinfo)))))

(define catbird
  (let ((commit "fa6a85b92f818f1e0139a33ca4110acc97f8d4f2"))
    (package
      (name "catbird")
      (version (string-append "0.1.0-1." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.dthompson.us/catbird.git")
                      (commit commit)))
                (sha256
                 (base32
                  "0jhsf1idrq433x4l10yfzkmd68k5y1ypsfzqpmh1rv6clczxyszf"))))
      (build-system gnu-build-system)
      (arguments
       '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
      (native-inputs (list autoconf automake pkg-config texinfo))
      (inputs (list guile-3.0))
      (propagated-inputs (list chickadee guile-sdl2))
      (synopsis "Game engine for Scheme programmers")
      (description "Catbird is a game engine written in Guile Scheme.")
      (home-page "https://dthompson.us/projects/chickadee.html")
      (license license:gpl3+))))

(package
  (name "super-bloom")
  (version "0.1.0-git")
  (source (git-checkout (url (dirname (current-filename)))))
  (build-system gnu-build-system)
  (arguments
   '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
  (native-inputs
   (list autoconf automake pkg-config))
  (inputs
   (list guile-3.0))
  (propagated-inputs
   (list catbird))
  (synopsis "Spring Lisp Game Jam 2023")
  (description "Dave's entry to the Spring Lisp Game Jam 2023")
  (home-page "https://dthompson.us")
  (license license:asl2.0))
