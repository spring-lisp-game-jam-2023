# Super Bloom

Super Bloom is a tiny game made for the [Spring Lisp Game Jam
2023](https://itch.io/jam/spring-lisp-game-jam-2023) using [Guile
Scheme](https://gnu.org/s/guile), the
[Chickadee](https://dthompson.us/projects/chickadee.html) game
library, and the experimental
[Catbird](https://git.dthompson.us/catbird.git/) game engine.

## Using Guix

We *highly recommend* using [Guix](https://guix.gnu.org/) if you want
to build this game from source.  Once Guix is installed, getting all
of the dependencies you need to build the game is *easy*.  Just run
`guix shell` from the root directory of this repository and you'll be
good to go!

## Manually installing dependencies

If you really want to go this route, you can.  You need:

- [Guile](https://www.gnu.org/software/guile/) 3.0.0+
- [Guile-OpenGL](https://www.gnu.org/software/guile-opengl/) 0.2.0+
- [Guile-SDL2](https://dthompson.us/projects/guile-sdl2.html) 0.8.0+
- [Chickadee](https://dthompson.us/projects/chickadee.html) 0.10.0+
  (building from Git master branch recommended as of this writing)
- [Catbird](https://git.dthompson.us/catbird.git/) built from Git main
  branch as it hasn't been officially released

## Building from source

Assuming all the dependencies have been installed on your system, the
game can be built like so:

```
./boostrap.sh
./configure
make -j$(nproc)
```

## Running the game

To run the game from the source checkout, use the `pre-inst-env` wrapper script:

```
./pre-inst-env super-bloom
```
