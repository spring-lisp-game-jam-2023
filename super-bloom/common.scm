;;; Copyright 2023 David Thompson
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (super-bloom common)
  #:use-module (catbird asset)
  #:use-module (catbird mixins)
  #:use-module (catbird scene)
  #:use-module (chickadee)
  #:use-module (chickadee audio)
  #:use-module (chickadee graphics text)
  #:use-module (oop goops)
  #:use-module (super-bloom config)
  #:export (%default-width
            %default-height
            %game-width
            %game-height
            %game-width:float
            %game-height:float
            scope-datadir
            monogram-font
            explosion-sound
            absorb-sound
            watered-sound
            spray-sound
            random:float
            steps
            <game-scene>
            water))

(define %default-width 640)
(define %default-height 480)
(define %game-width 320)
(define %game-height 240)
(define %game-width:float (exact->inexact %game-width))
(define %game-height:float (exact->inexact %game-height))

(define (scope-datadir file-name)
  (let ((prefix (or (getenv "SUPERBLOOM_DATADIR") %datadir)))
    (string-append prefix "/" file-name)))

(define-asset (monogram-font (file (scope-datadir "fonts/monogram_extended.ttf")))
  (load-font file 12 #:smooth? #f))

(define-asset (explosion-sound (file (scope-datadir "audio/explosion.wav")))
  (load-audio file))

(define-asset (absorb-sound (file (scope-datadir "audio/absorb.wav")))
  (load-audio file))

(define-asset (watered-sound (file (scope-datadir "audio/watered.wav")))
  (load-audio file))

(define-asset (spray-sound (file (scope-datadir "audio/spray.wav")))
  (load-audio file))

(define (random:float n)
  (* (random:uniform) n))

(define (steps n)
  (* n (current-timestep)))

(define-class <game-scene> (<scene>))

(define-method (width (scene <game-scene>))
  %game-width:float)

(define-method (height (scene <game-scene>))
  %game-height:float)

(define-accessor water)
