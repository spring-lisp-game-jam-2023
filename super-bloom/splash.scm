;;; Copyright 2023 David Thompson
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (super-bloom splash)
  #:use-module (catbird)
  #:use-module (catbird asset)
  #:use-module (catbird camera)
  #:use-module (catbird kernel)
  #:use-module (catbird mixins)
  #:use-module (catbird mode)
  #:use-module (catbird node)
  #:use-module (catbird node-2d)
  #:use-module (catbird region)
  #:use-module (catbird scene)
  #:use-module (chickadee data quadtree)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics path)
  #:use-module (chickadee graphics texture)
  #:use-module (chickadee math rect)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (oop goops)
  #:use-module (super-bloom common)
  #:use-module (super-bloom game)
  #:export (<splash-screen-mode>))

(define-asset (chickadee-texture (file (scope-datadir "images/chickadee.png")))
  (load-image file))

(define-class <splash-screen-mode> (<major-mode>))

(define-method (go-to-game (mode <splash-screen-mode>))
  (let ((region (find-region-by-name 'main))
        (scene (make <game-scene> #:name 'super-bloom)))
    (replace-scene region scene)
    (replace-major-mode scene (make <super-bloom-mode>))))

(define-method (on-enter (mode <splash-screen-mode>))
  (define (full-rect color)
    (with-style ((fill-color color))
      (fill (rectangle (vec2 0.0 0.0)
                       %game-width:float
                       %game-height:float))))
  (let ((scene (parent mode))
        (bg (make <canvas>
              #:name 'background
              #:rank 0
              #:painter (full-rect white)))
        (fader (make <canvas>
                 #:name 'fader
                 #:rank 999
                 #:width %game-width:float
                 #:height %game-height:float))
        (sprite (make <sprite>
                  #:name 'sprite
                  #:rank 1
                  #:texture chickadee-texture
                  #:origin (vec2 8.0 8.0)
                  #:scale 0.5))
        (label1 (make <label>
                  #:name 'built-with-label
                  #:rank 1
                  #:font monogram-font
                  #:color black
                  #:text "Built with Chickadee"))
        (label2 (make <label>
                  #:name 'url-label
                  #:rank 1
                  #:font monogram-font
                  #:color black
                  #:text "https://dthompson.us/projects/chickadee.html")))
    (run-script scene
      (attach-to scene bg fader sprite label1 label2)
      (center-in-parent sprite)
      (center-horizontal-in-parent label1)
      (center-horizontal-in-parent label2)
      (place-below sprite label1 #:padding 16.0)
      (place-below label1 label2 #:padding 16.0)
      (sleep 2.0)
      (tween 1.0 (make-color 0.0 0.0 0.0 0.0001) (make-color 0.0 0.0 0.0 1.0)
             (lambda (color)
               (set! (painter fader) (full-rect color)))
             #:interpolate color-lerp)
      (go-to-game mode))))
