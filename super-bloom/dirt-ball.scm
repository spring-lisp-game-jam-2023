;;; Copyright 2023 David Thompson
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (super-bloom dirt-ball)
  #:use-module (catbird asset)
  #:use-module (catbird node)
  #:use-module (catbird node-2d)
  #:use-module (chickadee audio)
  #:use-module (chickadee graphics particles)
  #:use-module (chickadee graphics texture)
  #:use-module (chickadee math)
  #:use-module (chickadee math rect)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (oop goops)
  #:use-module (super-bloom actor)
  #:use-module (super-bloom common)
  #:use-module (super-bloom flower)
  #:use-module (super-bloom water)
  #:export (<dirt-ball>
            dead?))

(define-asset (dirt-ball-tileset (file (scope-datadir "images/dirt-ball.png")))
  (load-tileset file 32 32))

(define-class <dirt-ball> (<actor>)
  (flower #:accessor flower #:init-keyword #:flower)
  (speed #:accessor speed #:init-keyword #:speed)
  (dead? #:accessor dead? #:init-value #f)
  (hitbox #:getter hitbox #:init-form (make-rect -8.0 -8.0 16.0 16.0))
  (explosion-particles #:accessor explosion-particles #:init-keyword #:explosion-particles))

(define-method (initialize (dirt-ball <dirt-ball>) initargs)
  (next-method)
  (attach-to dirt-ball
             (make <animated-sprite>
               #:name 'sprite
               #:atlas dirt-ball-tileset
               #:origin (vec2 16.0 16.0)
               #:animations `((default . ,(make <animation>
                                            #:frames #(0 1)
                                            #:frame-duration 0.3))
                              (up . ,(make <animation>
                                       #:frames #(2 3)
                                       #:frame-duration 0.3)))))
  (let ((dir (direction-to dirt-ball (flower dirt-ball))))
    (change-velocity dirt-ball
                     (* (vec2-x dir) (speed dirt-ball))
                     (* (vec2-y dir) (speed dirt-ball))))
  (update-animation dirt-ball))

(define-method (update-animation (dirt-ball <dirt-ball>))
  (let ((dy (vec2-y (velocity dirt-ball))))
    (change-animation (& dirt-ball sprite) (if (> dy 0.0) 'up 'default))))

(define-method (on-splash (dirt-ball <dirt-ball>))
  (set! (dead? dirt-ball) #t))

(define-method (on-collide (dirt-ball <dirt-ball>) (flower <flower>))
  (damage flower 2)
  (set! (dead? dirt-ball) #t))

(define-method (update (dirt-ball <dirt-ball>) _dt)
  (next-method)
  (when (dead? dirt-ball)
    (let ((p (position dirt-ball)))
      (audio-play (artifact explosion-sound))
      (add-particle-emitter (particles (explosion-particles dirt-ball))
                            (make-particle-emitter (make-rect (vec2-x p) (vec2-y p) 0.0 0.0)
                                                   40 3))
      (detach dirt-ball))))
