;;; Copyright 2023 David Thompson
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (super-bloom water)
  #:use-module (catbird asset)
  #:use-module (catbird node)
  #:use-module (catbird node-2d)
  #:use-module (chickadee audio)
  #:use-module (chickadee graphics particles)
  #:use-module (chickadee graphics texture)
  #:use-module (chickadee math)
  #:use-module (chickadee math rect)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (oop goops)
  #:use-module (super-bloom actor)
  #:use-module (super-bloom common)
  #:export (<water>
            absorbed?
            absorb!
            on-splash))

(define-asset (water-tileset (file (scope-datadir "images/water.png")))
  (load-tileset file 32 32))

(define-class <water> (<actor>)
  (hitbox #:getter hitbox #:init-form (make-rect -16.0 -16.0 32.0 32.0))
  (absorbed? #:accessor absorbed? #:init-value #f))

(define-method (initialize (water <water>) initargs)
  (next-method)
  (attach-to water
             (make <animated-sprite>
               #:name 'sprite
               #:atlas water-tileset
               #:origin (vec2 16.0 16.0)
               #:animations `((default . ,(make <animation>
                                            #:frames #(0 1)
                                            #:frame-duration 0.3))))))

(define-method (absorb! (water <water>))
  (set! (absorbed? water) #t))

(define-method (update (water <water>) _dt)
  (when (absorbed? water) (detach water))
  (next-method))

(define-method (on-splash (actor <actor>))
  #t)
